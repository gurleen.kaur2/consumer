package com.consumer.model;

public enum Operation {
    ADD,
    SUBTRACT,
    MULTIPLY
}
