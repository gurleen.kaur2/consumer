package com.consumer.controller;

import com.consumer.model.Input;
import com.consumer.model.Operation;
import com.consumer.model.Output;
import com.consumer.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/calculator-orchestrator")
public class CalculatorController {
    @Autowired
    private CalculatorService calculatorService;

    @PostMapping("/")
    public ResponseEntity<Output> calculate(@RequestBody Input input) {

        if (input.getOperation() == Operation.ADD) {
            Output output = calculatorService.add(input.getNumber1(), input.getNumber2());
            return ResponseEntity.ok(output);
        }

        return ResponseEntity.internalServerError().body(new Output("-1"));
    }
}
