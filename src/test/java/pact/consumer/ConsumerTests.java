package pact.consumer;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.PactSpecVersion;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import com.consumer.model.CalculationRequest;
import com.consumer.model.Operation;
import com.consumer.model.Output;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "calculator-service", pactVersion = PactSpecVersion.V3)
public class ConsumerTests {

    @Pact(consumer = "orchestrator", provider = "calculator-service")
    public RequestResponsePact createPactForAdd(PactDslWithProvider builder) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        CalculationRequest input = new CalculationRequest(3, 2);
        Output output = new Output("5");

        return builder
                .given("Add Operation")
                .uponReceiving("a request to add numbers")
                .path("/api/v1/calculator/add")
                .method(POST.name())
                .headers(Map.of("Content-Type", "application/json"))
                .body(objectMapper.writeValueAsString(input))
                .willRespondWith()
                .status(HttpStatus.OK.value())
                .body(objectMapper.writeValueAsString(output))
                .toPact();
    }

    @Test
    @PactTestFor(providerName = "calculator-service", pactMethod = "createPactForAdd")
    void shouldGetAddValue(MockServer mockServer) {
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        CalculationRequest calculationRequest = new CalculationRequest(3, 2);
        HttpHeaders headers = new HttpHeaders();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(List.of(APPLICATION_JSON, APPLICATION_OCTET_STREAM));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);
        HttpEntity<CalculationRequest> httpRequest = new HttpEntity<>(calculationRequest, headers);

        ResponseEntity<Output> responseEntity = restTemplate.exchange(
                mockServer.getUrl() + "/api/v1/calculator/add", POST, httpRequest, Output.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResult()).isEqualTo("5");
    }

}